const img = document.querySelectorAll("#im");
let imagesValue = 0;
let intervalId = null;

const btnStop = document.getElementById("stop");
btnStop.addEventListener("click", stopShow);
const btnStart = document.getElementById("start");
btnStart.addEventListener("click", startShow);

function nextImages() {
  img[imagesValue].classList.add("hidden");
  imagesValue = (imagesValue + 1) % img.length;
  img[imagesValue].classList.remove("hidden");
}

function startShow() {
  if (intervalId) {
    return;
  }
  intervalId = setInterval(nextImages, 3000);
  console.log(intervalId);
}

function stopShow() {
  console.log(intervalId);
  if (!intervalId) {
    return;
  }
  clearInterval(intervalId);
  intervalId = null;
}
startShow();
